package com.moviesroad

import grails.plugin.springsecurity.rest.RestTokenValidationFilter

import javax.servlet.FilterChain
import javax.servlet.ServletException
import javax.servlet.ServletRequest
import javax.servlet.ServletResponse

class CustomRestTokenValidationFilter extends RestTokenValidationFilter {

    @Override
    void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        chain.doFilter(request, response)
    }
}
