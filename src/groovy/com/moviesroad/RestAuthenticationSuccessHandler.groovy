package com.moviesroad

import grails.converters.JSON
import grails.transaction.Transactional
import org.codehaus.groovy.grails.web.json.JSONObject
import org.springframework.security.core.Authentication
import org.springframework.security.web.authentication.AuthenticationSuccessHandler

import javax.servlet.ServletException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Transactional
class RestAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

    @Override
    void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        response.contentType = "application/json"
        response.characterEncoding = "UTF-8"
        response.addHeader("Cache-Control", "no-store")
        response.addHeader("Pragma", "no-cache")
        response.setStatus(HttpServletResponse.SC_OK)
        User user = User.findById(authentication?.principal?.id as Long)
        def responseObj = user.toJson() as JSONObject
        responseObj.put("access_token", authentication?.accessToken)
        PrintWriter writer = response.getWriter();
        writer.print(responseObj as JSON)
    }
}
