package com.moviesroad

import grails.converters.JSON
import grails.util.Holders
import groovy.util.logging.Slf4j
import org.springframework.security.core.AuthenticationException
import org.springframework.security.web.authentication.AuthenticationFailureHandler

import javax.servlet.ServletException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Slf4j
class RestAuthenticationFailureHandler implements AuthenticationFailureHandler {

    void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
        def context = Holders.applicationContext
        def locale = Locale.getDefault()
        def messages = []

        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED)
        response.setContentType("application/json; charset=UTF-8")

        messages += ["message": context.getMessage('auth.bad.credentials', null, locale),
                     "code"   : context.getMessage('error.auth.code', null, locale as Locale)]

        def responseObj = ["errors" : messages] as JSON

        PrintWriter writer = response.getWriter()
        writer.print(responseObj)
    }
}
