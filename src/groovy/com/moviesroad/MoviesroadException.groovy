package com.moviesroad

class MoviesroadException extends RuntimeException {

    def messages = []

    def httpCode

    def setMessages(messages) {
        this.messages = messages
    }

    def setHttpCode(httpCode) {
        this.httpCode = httpCode
    }

    MoviesroadException() {}
}
