package com.moviesroad

import grails.converters.JSON

class BaseController {

    protected def retrieveErrorMessage(errorCodeEntry, errorMessageEntry) {

        def errorMessage = ["errors": [
                [
                        "message": message(code: errorMessageEntry),
                        "code": message(code: errorCodeEntry) as Integer
                ]
        ]]

        errorMessage = errorMessage as JSON

        return errorMessage
    }
}
