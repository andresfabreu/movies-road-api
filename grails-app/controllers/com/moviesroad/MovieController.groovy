package com.moviesroad

import grails.converters.JSON

import javax.servlet.http.HttpServletResponse

class MovieController extends BaseController {

    static allowedMethods = [
            create: "POST",
            update: "PUT",
            delete: "DELETE",
            list: "GET",
            index: "GET"
    ]

    static responseFormats = ["json"]

    def movieService

    def create() {
        def messages = [], httpCode

        if(!ValidationUtility.instance.validateJSON(request)){
            render(status: HttpServletResponse.SC_BAD_REQUEST, text: retrieveErrorMessage("error.validation.code", "json.invalid"), contentType: "application/json")
            return
        }

        try {
            Movie movie = movieService.create(request)
            render(status: HttpServletResponse.SC_CREATED, text: movie.toJson() as JSON, contentType: "application/json")
            return
        }
        catch(MoviesroadException ex) {
            messages += ex.getMessages()
            httpCode = ex.getHttpCode()
        }

        render(status: httpCode, text: ["errors":  messages] as JSON, contentType: "application/json")
    }

    def update() {
        def messages = [], httpCode

        if(!ValidationUtility.instance.validateJSON(request)){
            render(status: HttpServletResponse.SC_BAD_REQUEST, text: retrieveErrorMessage("error.validation.code", "json.invalid"), contentType: "application/json")
            return
        }

        try {
            Movie movie = movieService.update(request)
            render(status: HttpServletResponse.SC_OK, text: movie.toJson() as JSON, contentType: "application/json")
            return
        }
        catch(MoviesroadException ex) {
            messages += ex.getMessages()
            httpCode = ex.getHttpCode()
        }

        render(status: httpCode, text: ["errors":  messages] as JSON, contentType: "application/json")
    }

    def delete() {
        def messages = [], httpCode

        if(!ValidationUtility.instance.validateJSON(request)){
            render(status: HttpServletResponse.SC_BAD_REQUEST, text: retrieveErrorMessage("error.validation.code", "json.invalid"), contentType: "application/json")
            return
        }

        try {
            movieService.delete(params, request)
            render(status: HttpServletResponse.SC_OK, contentType: "application/json")
            return
        }
        catch(MoviesroadException ex) {
            messages += ex.getMessages()
            httpCode = ex.getHttpCode()
        }

        render(status: httpCode, text: ["errors":  messages] as JSON, contentType: "application/json")
    }

    def list() {
        def messages = [], httpCode, resp

        if(!ValidationUtility.instance.validateJSON(request)){
            render(status: HttpServletResponse.SC_BAD_REQUEST, text: retrieveErrorMessage("error.validation.code", "json.invalid"), contentType: "application/json")
            return
        }

        try {
            resp = movieService.list(params, request)

            render(status: HttpServletResponse.SC_OK, text: resp as JSON, contentType: "application/json")
            return
        }
        catch(MoviesroadException ex) {
            messages += ex.getMessages()
            httpCode = ex.getHttpCode()
        }

        render(status: httpCode, text: ["errors":  messages] as JSON, contentType: "application/json")
    }

    def index() {
        def messages = [], httpCode

        if(!ValidationUtility.instance.validateJSON(request)){
            render(status: HttpServletResponse.SC_BAD_REQUEST, text: retrieveErrorMessage("error.validation.code", "invalid.json"), contentType: "application/json")
            return
        }

        try {
            Movie movie = movieService.getById(params, request)

            render(status: HttpServletResponse.SC_OK, text: movie.toJson() as JSON, contentType: "application/json")
            return
        }
        catch(MoviesroadException ex) {
            messages += ex.getMessages()
            httpCode = ex.getHttpCode()
        }

        render(status: httpCode, text: ["errors":  messages] as JSON, contentType: "application/json")
    }
}