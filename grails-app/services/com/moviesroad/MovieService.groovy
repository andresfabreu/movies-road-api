package com.moviesroad

import grails.transaction.Transactional
import org.codehaus.groovy.grails.web.json.JSONObject

import javax.servlet.http.HttpServletResponse

@Transactional
class MovieService extends BaseService {

    def userService

    def create(request) {
        def data = request.JSON as JSONObject, user = request.user

        Movie movie = new Movie()
        setMovieData(movie, user, data)
        save(movie)

        return movie
    }

    def update(request) {
        def data = request.JSON as JSONObject, messages = [], httpCode, user = request.user

        Movie movie = Movie.findById(safeLong(data.id))

        if (movie == null) {
            messages += generateMessage('movie.not.found', 'error.entity.not.found.code')
            httpCode = HttpServletResponse.SC_BAD_REQUEST
        } else {
            setMovieData(movie, user, data)
            save(movie)

            return movie
        }

        ExceptionUtility.instance.setMessages(httpCode, messages)
    }

    def delete(params, request) {
        def messages = [], httpCode, user = request.user

        Movie movie = Movie.findByIdAndUser(safeLong(params.id), user)

        if (movie == null) {
            messages += generateMessage('movie.not.found', 'error.entity.not.found.code')
            httpCode = HttpServletResponse.SC_BAD_REQUEST
        } else {
            safeDelete(movie)

            return true
        }

        ExceptionUtility.instance.setMessages(httpCode, messages)
    }

    def getById(params, request) {
        def messages = [], httpCode, user = request.user

        Movie movie = Movie.findByIdAndUser(safeLong(params.id), user)

        if (movie == null) {
            messages += generateMessage('movie.not.found', 'error.entity.not.found.code')
            httpCode = HttpServletResponse.SC_BAD_REQUEST
        } else {
            return movie
        }

        ExceptionUtility.instance.setMessages(httpCode, messages)
    }

    def list(params, request) {
        def pageNumber, offset, max, movies = [], userFromRequest = request.user

        pageNumber = !params.page_number || safeInteger(params.page_number) <= 0 ? safeInteger(Movie.NO_PAGINATE) : safeInteger(params.page_number)
        offset = (pageNumber - 1) * Movie.NUMBER_OF_MOVIES_PER_PAGE
        max = pageNumber == Movie.NO_PAGINATE ? pageNumber : Movie.NUMBER_OF_MOVIES_PER_PAGE

        def movieList = Movie.createCriteria().list(max: max, offset: offset) {
            user {
                eq("id", userFromRequest.id)
            }

            if (params.title != null && !safeString(params.title).isEmpty()) {
                like("title", '%' + safeString(params.title) + '%')
            }

            order("title", "asc")
        }

        movieList.each { movie ->
            movies += movie.toShortJson()
        }

        return [
                "movies"           : movies,
                "have_another_page": movieList.size() > 0 && Math.abs(pageNumber) * movieList.size() < movieList.getTotalCount(),
                "current_page"     : pageNumber != Movie.NO_PAGINATE ? pageNumber : 1,
                "number_of_pages"  : pageNumber != Movie.NO_PAGINATE && movieList.getTotalCount() > 0 ? Math.ceil(movieList.getTotalCount() / Movie.NUMBER_OF_MOVIES_PER_PAGE) : 1
        ]
    }

    def setMovieData(movie, user, data) {
        movie.title = data.title != null ? safeString(data.title) : null
        movie.description = data.description != null ? safeString(data.description) : null
        movie.coverImageUrl = data.cover_image_url != null ? safeString(data.cover_image_url) : false
        movie.detailImageUrl = data.detail_image_url != null ? safeString(data.detail_image_url) : null
        userService.addMovieToUser(movie, user)

        return true
    }

    def save(movie) {
        validate(movie, locale)
        safeSave(movie)
    }

}
