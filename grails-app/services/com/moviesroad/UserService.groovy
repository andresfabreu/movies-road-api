package com.moviesroad

import grails.transaction.Transactional

import javax.servlet.http.HttpServletResponse

@Transactional
class UserService extends BaseService {

    def addMovieToUser(Movie movie, User user) {
        user.addToMovies(movie)
        validate(user, locale)
        safeSave(user)
    }

    def retrieveSessionUser(Long userId, List<String> roles) {
        def messages = [], httpCode, authorized = false

        User user = User.findById(userId)

        if(user == null) {
            messages += generateMessage('user.not.found', 'error.entity.not.found.code')
            httpCode = HttpServletResponse.SC_BAD_REQUEST
        }
        else {
            / * CHECK USER ROLES */
            roles.each { role ->
                user.getAuthorities().each { userRole ->
                    if(userRole.getAuthority() == role){
                        authorized = true
                    }
                }
            }

            if(authorized || !roles) {
                return user
            }
            else {
                messages += generateMessage('access.forbidden', 'error.access.forbidden.code')
                httpCode = HttpServletResponse.SC_FORBIDDEN
            }
        }

        ExceptionUtility.instance.setMessages(httpCode, messages)
    }
}
