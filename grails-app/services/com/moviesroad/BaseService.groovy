package com.moviesroad

import grails.transaction.Transactional
import org.codehaus.groovy.grails.plugins.DomainClassGrailsPlugin
import org.springframework.dao.DuplicateKeyException

import javax.servlet.http.HttpServletResponse
import java.sql.SQLException

@Transactional
class BaseService {

    def messageSource
    def locale = new Locale("pt", "BR")
    def sessionFactory
    def propertyInstanceMap = DomainClassGrailsPlugin.PROPERTY_INSTANCE_MAP

    protected validate(objectInstance, locale) {

        def messages = []

        if (!objectInstance.validate()) {
            for (fieldErrors in objectInstance.errors) {
                for (error in fieldErrors.allErrors) {
                    messages += ["message": messageSource.getMessage(error, locale),
                                 "code"   : messageSource.getMessage("error.validation.code", null, locale as Locale)]
                }
            }
        }
        else {
            return true
        }

        ExceptionUtility.instance.setMessages(HttpServletResponse.SC_BAD_REQUEST, messages)
    }

    protected safeSave(objectInstance) {

        def messages = [], httpCode = HttpServletResponse.SC_INTERNAL_SERVER_ERROR

        try {
            if (!objectInstance.save(flush: true, validate: true)) {
                objectInstance.errors.allErrors.each { error ->
                    httpCode = HttpServletResponse.SC_BAD_REQUEST
                    messages += generateMessage("database.save.error", "error.database.code")
                }
            }
            else return objectInstance
        }
        catch (DuplicateKeyException ignored) {
            httpCode = HttpServletResponse.SC_CONFLICT
            messages += generateMessage("database.duplicated.key", "error.database.code")
        }
        catch (SQLException ignored) {
            messages += generateMessage("database.save.error", "error.database.code")
        }

        ExceptionUtility.instance.setMessages(httpCode, messages)
    }

    protected safeDelete(objectInstance) {
        def messages = [], httpCode = HttpServletResponse.SC_INTERNAL_SERVER_ERROR

        try {
            objectInstance.delete(flush: true)
            return true
        }
        catch (Exception ignored) {
            messages += generateMessage("database.delete.error", "error.database.code")
        }

        ExceptionUtility.instance.setMessages(httpCode, messages)
    }

    protected generateMessage(message, code) {
        return ["message": messageSource.getMessage(message, null, locale),
                "code"   : messageSource.getMessage(code, null, locale as Locale)]
    }

    protected generateMessage(message, args, code) {
        return ["message": messageSource.getMessage(message, args, locale),
                "code"   : messageSource.getMessage(code, null, locale as Locale)]
    }


    protected safeLong(data) {

        Long dataLong
        def messages = []

        try {
            dataLong = Long.valueOf(data)
            return dataLong
        }
        catch (Exception ignored) {
            messages += generateMessage("convert.long.error", "error.convert.code")
        }

        ExceptionUtility.instance.setMessages(HttpServletResponse.SC_BAD_REQUEST, messages)
    }

    protected safeString(data) {

        String dataString
        def messages = []

        try {
            if(data == null) throw new Exception()

            dataString = String.valueOf(data)
            return dataString
        }
        catch (Exception ignored) {
            messages += generateMessage("convert.string.error", "error.convert.code")
        }

        ExceptionUtility.instance.setMessages(HttpServletResponse.SC_BAD_REQUEST, messages)
    }

    protected safeBoolean(data) {

        Boolean dataBoolean
        def messages = []

        try {
            if(data == null) data = 55

            dataBoolean = Boolean.valueOf(data)
            return dataBoolean
        }
        catch (Exception ignored) {
            messages += generateMessage("convert.boolean.error", "error.convert.code")
        }

        ExceptionUtility.instance.setMessages(HttpServletResponse.SC_BAD_REQUEST, messages)
    }

    protected safeDouble(data) {

        Double dataDouble
        def messages = []

        try {
            dataDouble = Double.valueOf(data)
            return dataDouble
        }
        catch (Exception ignored) {
            messages += generateMessage("convert.double.error", "error.convert.code")
        }

        ExceptionUtility.instance.setMessages(HttpServletResponse.SC_BAD_REQUEST, messages)
    }

    protected safeInteger(data) {

        Integer dataInteger
        def messages = []

        try {
            dataInteger = Integer.valueOf(data)
            return dataInteger
        }
        catch (Exception ignored) {
            messages += generateMessage("convert.integer.error", "error.convert.code")
        }

        ExceptionUtility.instance.setMessages(HttpServletResponse.SC_BAD_REQUEST, messages)
    }

    /* CLEAR SESSION FOR BULK INSERT */
    protected cleanUpGorm() {
        def session = sessionFactory.currentSession
        session.flush()
        session.clear()
        propertyInstanceMap.get().clear()
    }
}
