package movies.road.api

import com.moviesroad.MoviesroadException
import com.moviesroad.Role
import grails.converters.JSON
import grails.plugin.springsecurity.rest.token.storage.TokenStorageService
import org.springframework.security.core.AuthenticationException
import org.springframework.security.core.userdetails.UserDetails

import javax.servlet.http.HttpServletResponse

class AppFilters {

    def userService
    TokenStorageService tokenStorageService

    def filters = {
        all(controller:'movie', action:'*') {
            before = {
                List allowedRoles = [Role.ROLE_USER] as ArrayList
                def errors = []

                if(!request.getHeader('Authorization')) {
                    errors += userService.generateMessage('not.authenticated', 'error.auth.code')
                    render(status: HttpServletResponse.SC_UNAUTHORIZED, text: ["errors":  errors] as JSON, contentType: 'application/json')
                    return false
                }
                try {
                    UserDetails userDetails = tokenStorageService.loadUserByToken(request.getHeader('Authorization'))
                    request.user = userService.retrieveSessionUser(userDetails.getId(), allowedRoles)
                    return true
                }
                catch(AuthenticationException ignored) {
                    errors += userService.generateMessage('not.authenticated', 'error.auth.code')
                    render(status: HttpServletResponse.SC_UNAUTHORIZED, text: ["errors":  errors] as JSON, contentType: 'application/json')
                    return false
                }
                catch(MoviesroadException ex) {
                    render(status: ex.getHttpCode(), text: ["errors":  ex.getMessages()] as JSON, contentType: 'application/json')
                    return false
                }
            }
            after = { Map model ->

            }
            afterView = { Exception e ->

            }
        }
    }
}
