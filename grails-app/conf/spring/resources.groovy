import com.moviesroad.CustomRestTokenValidationFilter
import com.moviesroad.RestAuthenticationFailureHandler
import com.moviesroad.RestAuthenticationSuccessHandler
beans = {
    restAuthenticationFailureHandler(RestAuthenticationFailureHandler)
    restAuthenticationSuccessHandler(RestAuthenticationSuccessHandler)
    restTokenValidationFilter(CustomRestTokenValidationFilter)
}
