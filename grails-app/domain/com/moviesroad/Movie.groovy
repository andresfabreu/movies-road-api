package com.moviesroad

class Movie {

    static final int NUMBER_OF_MOVIES_PER_PAGE = 10
    static final int NO_PAGINATE = -1

    String title
    String coverImageUrl
    String detailImageUrl
    String description

    static belongsTo = [
            User
    ]

    static hasOne = [
            user: User
    ]

    static constraints = {
        title nullable: false, blank: false, maxSize: 255
        coverImageUrl nullable: false, blank: false, maxSize: 255
        detailImageUrl nullable: false, blank: false, maxSize: 255
        description nullable: false, blank: false
        user nullable: false
    }

    static mapping = {
        description type: 'text'
    }

    def toShortJson() {

        return [
                "id": id,
                "title": title,
                "cover_image_url": "https://s3-sa-east-1.amazonaws.com/moviesroad/cover_img/" + coverImageUrl
        ]
    }

    def toJson() {

        return [
                "id": id,
                "title": title,
                "detail_image_url": "https://s3-sa-east-1.amazonaws.com/moviesroad/detail_img/" + detailImageUrl,
                "description": description
        ]
    }
}
