package com.moviesroad

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

@EqualsAndHashCode(includes='username')
@ToString(includes='username', includeNames=true, includePackage=false)
class User implements Serializable {

	private static final long serialVersionUID = 1

	static final String MOVIES_ROAD = 'moviesroad'

	transient springSecurityService

	String username
    String name
	String password
	boolean enabled = true
	boolean accountExpired
	boolean accountLocked
	boolean passwordExpired

	User(String username, String password) {
		this()
		this.username = username
		this.password = password
	}

	Set<Role> getAuthorities() {
		UserRole.findAllByUser(this)*.role
	}

	def beforeInsert() {
		encodePassword()
	}

	def beforeUpdate() {
		if (isDirty('password')) {
			encodePassword()
		}
	}

	protected void encodePassword() {
		password = springSecurityService?.passwordEncoder ? springSecurityService.encodePassword(password) : password
	}

	static transients = ['springSecurityService']

	static hasMany = [
	        movies: Movie
	]

	static constraints = {
        password nullable: false, blank: false, password: true
        username nullable: false, blank: false, unique: true, maxSize: 10
        name nullable: false, blank: false, maxSize: 50
		movies nullable: true
	}

	static mapping = {
		password column: '`password`'
	}

    def toJson() {
        def roleString = null
        List<Role> roleList = getAuthorities() != null && getAuthorities().size() > 0 ? getAuthorities().toList() : new ArrayList<>()
        roleList.each { role ->
            roleString = role.authority
        }

        return [
                "id": id,
                "name": name,
                "username": username,
                "role": roleString
        ]
    }
}
