package com.moviesroad

class AuthenticationToken {

    String tokenValue
    String username
    Long refreshed = new Date().getTime()

    def afterLoad() {
        refreshed = new Date().getTime()
        this.save()
    }

    static mapping = {
        version false
    }
}
