<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-js"><!--<![endif]-->

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title><g:layoutTitle default="MOVIES ROAD API"/></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<link rel="shortcut icon" href="${assetPath(src: 'favicon.ico')}" type="image/x-icon" />
	<link rel="apple-touch-icon" href="${assetPath(src: 'apple-touch-icon.png')}" />
	<link rel="apple-touch-icon" sizes="57x57" href="${assetPath(src: 'apple-touch-icon-57x57.png')}"/>
	<link rel="apple-touch-icon" sizes="72x72" href="${assetPath(src: 'apple-touch-icon-72x72.png')}"/>
	<link rel="apple-touch-icon" sizes="76x76" href="${assetPath(src: 'apple-touch-icon-76x76.png')}"/>
	<link rel="apple-touch-icon" sizes="114x114" href="${assetPath(src: 'apple-touch-icon-114x114.png')}"/>
	<link rel="apple-touch-icon" sizes="120x120" href="${assetPath(src: 'apple-touch-icon-120x120.png')}"/>
	<link rel="apple-touch-icon" sizes="144x144" href="${assetPath(src: 'apple-touch-icon-144x144.png')}"/>
	<link rel="apple-touch-icon" sizes="152x152" href="${assetPath(src: 'apple-touch-icon-152x152.png')}"/>
	<link rel="apple-touch-icon" sizes="180x180" href="${assetPath(src: 'apple-touch-icon-180x180.png')}"/>
	<link href="https://fonts.googleapis.com/css?family=Exo+2:300,300italic,400,400italic,500,500italic,600,600italic,700,700italic,800" rel="stylesheet" type="text/css">
	<asset:stylesheet src="application.css"/>
	<g:layoutHead/>
</head>
<body>
<div id="wrap">
	<div id="app-logo">
		<span>MOVIES ROAD API</span>
	</div>
	<g:layoutBody/>
</div>
</body>
</html>