<!DOCTYPE html>
<html>
<head>
	<meta name="layout" content="main"/>
	<asset:stylesheet src="index.css"/>
</head>
<body>
<div id="index-content">
    <img class="api-logo" src="${assetPath(src: 'base-api.png')}">
</div>
</body>
</html>
