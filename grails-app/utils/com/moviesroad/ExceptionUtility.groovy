package com.moviesroad

@Singleton
class ExceptionUtility {
    def exceptionMessages = [], code

    def setMessages(httpCode, messages) {
        if (httpCode != null && messages.size() != 0) {
            exceptionMessages.clear()
            exceptionMessages += messages
            this.code = httpCode
            def validationException = new MoviesroadException()
            validationException.setMessages(messages)
            validationException.setHttpCode(httpCode)
            throw validationException
        }
    }
}
