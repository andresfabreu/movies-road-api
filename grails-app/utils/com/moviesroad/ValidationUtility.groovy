package com.moviesroad

import grails.converters.JSON
import org.codehaus.groovy.grails.web.converters.exceptions.ConverterException
import org.codehaus.groovy.grails.web.json.JSONArray

@Singleton
class ValidationUtility {
    static def validateJSON(request) {
        try {
            def parsedRequest = JSON.parse(request)
            if(parsedRequest instanceof JSONArray){
                return false
            }
            return true
        }
        catch (ConverterException ignored) {
            return false
        }
    }
}
