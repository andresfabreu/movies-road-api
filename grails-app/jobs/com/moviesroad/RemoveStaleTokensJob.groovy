package com.moviesroad

class RemoveStaleTokensJob {

	static triggers = {
		cron name: 'removeStaleTokensJobEveryMidnight', cronExpression: "0 1/1 * * * ? *"
	}

	void execute() {

		Long millisInAWeek = 8L * 24L * 60L * 60L * 1000L
		Long millisDate = new Date().getTime() - millisInAWeek

		AuthenticationToken.executeUpdate("delete AuthenticationToken aut where aut.refreshed < '" + millisDate + "'")
	}
}